import sys
from os import listdir, getcwd
from os.path import join, basename, splitext, dirname
from numpy import expand_dims
from matplotlib import pyplot
from matplotlib.patches import Rectangle, Polygon
from mrcnn.config import Config
from mrcnn.model import MaskRCNN
from mrcnn.model import mold_image
import skimage
from imantics import Polygons, Mask

def load_image(full_path_to_image_file):
    """
    Load the specified image and return a [H,W,3] Numpy array.
    """
    image = skimage.io.imread(full_path_to_image_file)
    # If grayscale. Convert to RGB for consistency.
    if image.ndim != 3:
        image = skimage.color.gray2rgb(image)
    # If has an alpha channel, remove it for consistency
    if image.shape[-1] == 4:
        image = image[..., :3]
    return image


# define the prediction configuration
class PredictionConfig(Config):
    # define the name of the configuration
    NAME = "cancer_cfg"
    # number of classes (background + healthy + normal)
    NUM_CLASSES = 1 + 2
    # simplify GPU config
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    DETECTION_MIN_CONFIDENCE = 0
    DETECTION_MAX_INSTANCES = 1000

def plot_predicted(model, cfg, full_path_to_image_file,outdir):
    # load image and mask
    proliferative_cells_count=0
    non_proliferative_cells_count=0

    # load the image and mask
    image = load_image(full_path_to_image_file)
    # convert pixel values (e.g. center)
    scaled_image = mold_image(image, cfg)
    # convert image into one sample
    sample = expand_dims(scaled_image, 0)
    # make prediction
    yhat = model.detect(sample, verbose=0)[0]
    # define subplot
    pyplot.subplot(111)

    # turn off axis labels
    pyplot.axis('off')
    # plot raw pixel data
    pyplot.imshow(image)
    ax = pyplot.gca()
    # plot each box
    for k, box in enumerate(yhat['rois']):
        # get coordinates
        class_id=yhat['class_ids'][k]
        mask=yhat['masks'][:,:,k]
        polygon = Mask(mask).polygons()
        points = polygon.points
        color=None
        if class_id == 1:
            color='red'
            proliferative_cells_count+=1
        else:
            color='blue'
            non_proliferative_cells_count+=1
            # create the shape
        y1, x1, y2, x2 = box
        # calculate width and height of the box
        width, height = x2 - x1, y2 - y1
        # create the shape
        rect = Rectangle((x1, y1), width, height, fill=False, color=color)
        pol = Polygon(points[0], fill=False, color=color)
        # draw the box
        #ax.add_patch(rect)
        ax.add_patch(pol)

    ki67=proliferative_cells_count/(non_proliferative_cells_count + proliferative_cells_count)
    pyplot.title("Ki-67 {}%".format(ki67))
    image_file =basename(full_path_to_image_file)
    image_id = splitext(image_file)[0]
    pyplot.savefig(join(outdir,image_id+'.annotated.png'))
    pyplot.close()

if __name__ == '__main__':
    full_path_to_image_file=sys.argv[1]
    weights_file=sys.argv[2]
    outdir=sys.argv[3]

    cfg = PredictionConfig()
    model = MaskRCNN(mode='inference', model_dir=getcwd(), config=cfg)
    model.load_weights(weights_file, by_name=True)
    plot_predicted(model, cfg,full_path_to_image_file, outdir)