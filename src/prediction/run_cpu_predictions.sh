#!/bin/bash

#SBATCH --time=00:10:00

#SBATCH -p thin-shared

#SBATCH --mem=16GB
 
#SBATCH --job-name="predict"

#SBATCH --error="predict.%j.err"
#SBATCH --output="predict.%j.out"

export PATH=/mnt/software/miniconda3/bin:$PATH

python predict.py \
/mnt/images/cells.jpeg \
/mnt/weights/mask_rcnn_cancer_cfg_0099.h5 \
/mnt/images_annotated/
