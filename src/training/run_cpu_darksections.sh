#!/bin/bash

#SBATCH --time=24:00:00

#SBATCH -p thinnodes

#SBATCH -N 1
 
#SBATCH --job-name="cells"

#SBATCH --error="cells.%j.err"
#SBATCH --output="cells.%j.out"


export PATH=/mnt/software/miniconda3/bin:$PATH

python cells.py \
/mnt/fulldarkandcleardatasetsections \
/mnt/fullandcleardarkdatasetsections_model \
/mnt/fullandcleardarkdatasetsections_model/mask_rcnn_coco.h5