import sys
from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import asarray
from numpy import expand_dims
from matplotlib import pyplot
from matplotlib.patches import Rectangle, Polygon
from mrcnn.config import Config
from mrcnn.model import MaskRCNN
from mrcnn.model import mold_image
from mrcnn.utils import Dataset
import json
from mrcnn.utils import compute_ap
from mrcnn.model import load_image_gt
import numpy as np
import skimage
from mrcnn import visualize
import cv2
from imantics import Polygons, Mask
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from mrcnn.visualize import display_instances
import pickle
from PIL import Image
from matplotlib import pyplot as plt
import imgaug.augmenters as iaa


seq = iaa.Sequential(
    [
        iaa.Fliplr(0.5),
        iaa.Flipud(0.5),
        iaa.OneOf([ ## rotate
            iaa.Affine(rotate=0),
            iaa.Affine(rotate=90),
            iaa.Affine(rotate=180),
            iaa.Affine(rotate=270)]),
    ],
    random_order=True
)


class CancerDataset(Dataset):
    # load the dataset definitions
    def load_dataset(self, dataset_dir, is_train=True):
        # define one class
        self.add_class("dataset", 1, "cancer")
        self.add_class("dataset", 2, "healthy")
        # define data locations
        images_dir = dataset_dir + '/images/'
        annotations_dir = dataset_dir + '/annots/'
        # find all images

        for filename in listdir(annotations_dir):
            # extract image id
            image_id = filename[:-4]

            # skip bad images
            img_path = images_dir + image_id + 'jpg'
            ann_path = annotations_dir + image_id + 'json'
            # add to dataset
            self.add_image('dataset',
                           image_id=image_id,
                           path=img_path,
                           annotation=ann_path)

    # load all bounding boxes for an image
    def extract_masks(self, filename):
        with open(filename, "r") as rf:
            decoded_data = json.load(rf)

        h = decoded_data['imageHeight']
        w = decoded_data['imageWidth']
        masks = np.zeros([h , w , len(decoded_data['shapes'])], dtype='uint8')
        classes = []
        for i, shape in enumerate(decoded_data['shapes']):
            mask = np.zeros([h, w], dtype=np.uint8)
            if shape['label'] == 'cancer':
                cv2.fillPoly(mask, np.array([shape['points']], dtype=np.int32), 1)
                masks[:, :, i] = mask
                classes.append(self.class_names.index(shape['label']))
            if shape['label'] == 'healthy':
                cv2.fillPoly(mask, np.array([shape['points']], dtype=np.int32), 2)
                classes.append(self.class_names.index(shape['label']))
                masks[:, :, i] = mask

        return masks, classes

    # load the masks for an image
    def load_mask(self, image_id):
        # get details of image
        info = self.image_info[image_id]
        # define box file location
        path = info['annotation']
        # load XML
        masks, classes = self.extract_masks(path)
        return masks, np.asarray(classes, dtype='int32')

    # load an image reference
    def image_reference(self, image_id):
        info = self.image_info[image_id]
        return info['path']


# define a configuration for the model
class CancerConfig(Config):
    # define the name of the configuration
    NAME = "cancer_cfg"
    NUM_CLASSES = 1 + 2
    #STEPS_PER_EPOCH = 12
    STEPS_PER_EPOCH = 24 
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    DETECTION_MIN_CONFIDENCE = 0
    POST_NMS_ROIS_TRAINING = 1000
    POST_NMS_ROIS_INFERENCE = 2000
    MAX_GT_INSTANCES = 200
    DETECTION_MAX_INSTANCES = 400
    TRAIN_ROIS_PER_IMAGE = 128
    RPN_NMS_THRESHOLD = 0.9
    RPN_TRAIN_ANCHORS_PER_IMAGE = 64


if __name__ == '__main__':
    data_set=sys.argv[1]
    model_dir=sys.argv[2]
    weights=sys.argv[3]
    
    config = CancerConfig()
    train_set = CancerDataset()
    train_set.load_dataset(data_set, is_train=True)
    train_set.prepare()
    model = MaskRCNN(mode='training', model_dir=model_dir, config=config)
    model.load_weights(weights, by_name=True, exclude=["mrcnn_class_logits","mrcnn_bbox_fc", "mrcnn_bbox", "mrcnn_mask"])
    model.train(train_set, train_set, learning_rate=config.LEARNING_RATE, epochs=100,layers='heads',augmentation = seq)