# Cancer Cell Classification in Ki-67 microscope slides using Mask R CNN

## Motivation
This is a prototype to automate cell counting and cell classification in [Ki-67](https://en.wikipedia.org/wiki/Ki-67_(protein)) immunohistochemistry microscopy slides from cancer patients. Specifically the goal here is to count all the cells in an image as the one below and classify each cell as being 1) non-proliferating, Ki-67 negative (blue cells) or 2) proliferating, Ki-67 positive (brown cells). Currenlty this task is done mostly manually by a pathologist.

I expect that increasing the current training will likely make the model perform as well as a human does.

 ![concept](images/concept.png)

## training and prediction

I was given **five images for training (and prediction)** so I am using a squared section from each of them to train the 'heads' of this [Mask R CNN](https://github.com/matterport/Mask_RCNN) model implementation. These are the sections:

![concept](images/section0.jpeg)
![concept](images/section1.jpeg)
![concept](images/section2.jpeg)
![concept](images/section3.jpeg)
![concept](images/section4.jpeg)

I manually labeled each cell on the sections as belonging to one of the two cell states using the [labelme](https://github.com/wkentaro/labelme) tool. These are screenshots from the program, the json and jpeg files of the labeling step can be found [here](training/).

![concept](images/labelme.png)

The training script that uses the Mask R CNN code implementation is [here](src/training/) and the [pre-trained coco weights](https://cocodataset.org/). The details of the traning paramaters such us the number of epochs and data augmentation can be easily found in the script.

The weight files produded by the traning step can be found [here](https://drive.google.com/file/d/1nZRerXazsTSRbj1pzeDgisOkw634pTFX/view?usp=sharing).

I then used those weights to precit the cell types on the whole images. The prediction script can be found [here](src/prediction/).

These are the full five images with the predicted bounding boxes and masks. There is room for improvement but also a great in the potential to become suitable for production.

![concept](images/prediction1.jpeg)
![concept](images/prediction2.jpeg)
![concept](images/prediction3.jpeg)
![concept](images/prediction4.jpeg)
![concept](images/prediction5.jpeg)


## Notes on versions for Tensorflow, Keras and Mask R CNN:
	Tensorflow: 1.5.0
	Keras: 2.0.8
	Mask R CNN: https://github.com/matterport/Mask_RCNN/releases/tag/v2.1

	In file keras/models.py set:
		workers=1
		use_multiprocessing=False
	in:
		self.model.predict_generator(generator, steps,
		max_queue_size=max_queue_size,
		workers=1,
		use_multiprocessing=False,
		verbose=verbose)
	and:
		self.model.fit_generator(generator,
		steps_per_epoch,
		epochs,
		verbose=verbose,
		callbacks=callbacks,
		validation_data=validation_data,
		validation_steps=validation_steps,
		class_weight=class_weight,
		max_queue_size=max_queue_size,
		workers=1,
		use_multiprocessing=False,
		initial_epoch=initial_epoch)
	
	In file keras/engine/training.py, inside fit_generator function set:
		workers=1
		use_multiprocessing=False